# SwipeRx
SwipeRx project space

# Commands
Some commands in here to automate stuff

# CPD
Feature documentation for CPD

## `start-exam`
Start exam endpoint exists for the user to start an exam.

### Re-attempt
A user is allowed to re-attempt examinations. Given that the variable: `attempt_interval` (minutes required for the user to attempt the next `exam`). The `attempt_reset_interval` is also checked if the user is allowed to retake all of the attempts.

Things to note:

 * If `attempt_interval` is greater than `attempt_reset_interval`. `attempt_reset_interval` is practically useless.
 * All values are in minutes.

